package cn.codeforfun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoLightweightMysqlMigrateToolsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoLightweightMysqlMigrateToolsApplication.class, args);
	}

}
